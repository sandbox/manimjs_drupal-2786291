(function($) {
	$(document).ready(function() {

	   var address = '58 Southside Drive ,6025, Hillarys ,WA';
	   var geocoder = new google.maps.Geocoder();
	   geocoder.geocode({ 'address': address }, function (results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				
			} else {
			   // alert("Request failed.")
			} 
			
			
			var myCenter=new google.maps.LatLng(latitude,longitude);

			function initialize()
			{
				var mapProp = {
					center:myCenter,
					zoom:12,
					mapTypeId:google.maps.MapTypeId.ROADMAP
				};

				var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

				var marker=new google.maps.Marker({
				  position:myCenter,
				  });

				marker.setMap(map);
				  var infowindow = new google.maps.InfoWindow({
                  content: address,
				  maxWidth: 200

				});
				google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			  });
			  var request = {
				placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
			  };
				var service = new google.maps.places.PlacesService(map);
				 service.getDetails(request, function(placeResult, status) {
				if (status != google.maps.places.PlacesServiceStatus.OK) {
				  alert('Unable to find the Place details.');
				  return;
				}
				// Add a marker, using the Place Id and Location instead of the position
					// attribute.
					var marker = new google.maps.Marker({
					  map: map,
					  place: {
						placeId: placeResult.place_id,
						location: placeResult.geometry.location
					  },
					  // Attributions help users find your app again.
					  attribution: {
						source: 'Google Maps JavaScript API',
						webUrl: 'https://developers.google.com/maps/'
					  }
					});
					// Add an info window to the Marker. Here we use data returned in the Place
					// Details request to provide richer info window text.
					marker.addListener('click', function() {
					  infowindow.setContent('<a href="' + placeResult.website + '">' +
						placeResult.name + '</a>');
					  infowindow.open(map, this);
					});
				  });
				}
		google.maps.event.addDomListener(window, 'load', initialize);

			
			
			
		
		});
				
			 
    });

		
})(jQuery);